from django.template.loader import get_template
from django.template import Context
from django.core.context_processors import csrf
from django.http import HttpResponse

def home(request):
    t = get_template('index.html')
    c = Context({})
    return HttpResponse(t.render(c))