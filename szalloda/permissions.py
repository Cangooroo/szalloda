from rest_framework.permissions import BasePermission

class ReceptionistPermission(BasePermission):

    def has_permission(self, request, view):
        return request.user.has_perm('account.register_account')


class WaiterPermission(BasePermission):
    def has_permission(self, request, view):
        return True