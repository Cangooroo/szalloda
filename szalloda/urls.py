from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from account.api import administrative, payment, user, info


admin.autodiscover()

user_urls = patterns('',
    url(r'^login$', user.Login.as_view()),
    url(r'^logout$', user.Logout.as_view()),
)

info_urls = patterns('',
    url(r'^balance$', info.BalanceInfo.as_view()),
    url(r'^daily$', info.DailySummary.as_view()),
    url(r'^latest$', info.LatestTransaction.as_view()),
)

pay_urls = patterns('',
    url(r'^value$', payment.PayWithValue.as_view()),
    url(r'^image$', payment.PayWithImage.as_view()),
)

urlpatterns = patterns('',

    url(r'^storno$', administrative.Storno.as_view()),
    url(r'^topup$', administrative.Topup.as_view()),
    url(r'^withdraw$', administrative.Withdraw.as_view()),

    url(r'^info/', include(info_urls)),
    url(r'^user/', include(user_urls)),
    url(r'^pay/', include(pay_urls)),

    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)