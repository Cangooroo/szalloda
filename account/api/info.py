from rest_framework.views import APIView
from rest_framework.response import Response
import datetime


class BalanceInfo(APIView):
    """
    Egyenleg lekérdezés
    """

    def post(self, request):
        return Response({
            'success' : True,
            'amount' : 0
        })

    def metadata(self, request):
        """
        Az OPTIONS művelethez biztosít információkat.
        """
        data = super().metadata(request)

        name = data.pop('name')
        description = data.pop('description')

        return {
            'name': name,
            'description': description,
            'input':
                {
                    'device_id': {
                        'description': 'A PDA/terminal azonositoja',
                        'type': 'string'
                    },
                    'bracelet_id': {
                        'description': 'A karperec id-je',
                        'type': 'string'
                    }
                },
            'output':
                {
                    'success' : {
                        'description': 'A kifizetes sikeresseget jeloli.',
                        'type': 'boolean'
                    },
                    'amount' : {
                        'description': 'A szamlan levo osszeg.',
                        'type': 'int'
                    }
                }
            }


class FeeInfo(APIView):
    """
    Díj infó.
    """

    def post(self, request):
        return Response({
            'result': True
        })


class DailySummary(APIView):
    """
    A napi statisztikai adatok lekérdezése.
    """

    def post(self, request):
        return Response({
            'success' : True,

            'pay_amount' : 0,
            'pay_count' : 0,

            'storno_amount' : 0,
            'storno_count' : 0,

            'cash_topup_amount' : 0,
            'cash_topup_count' : 0,

            'card_topup_amount' : 0,
            'card_topup_count' : 0,

            'refund_amount' : 0,
            'refund_count' :0
        })

    def metadata(self, request):
        """
        Az OPTIONS művelethez biztosít információkat.
        """
        data = super().metadata(request)

        name = data.pop('name')
        description = data.pop('description')

        return {
            'name': name,
            'description': description,
            'input':
                {
                    'device_id': {
                        'description': 'A PDA/terminal azonositoja',
                        'type': 'string'
                    }
                },
            'output':
                {
                    'pay_amount' : {
                        'description': 'A vasarlasok osszege.',
                        'type': 'int'
                    },
                    'pay_count' : {
                        'description': 'A vasarlasok szama.',
                        'type': 'int'
                    },
                    'storno_amount' : {
                        'description': 'A sztornok osszege.',
                        'type': 'int'
                    },
                    'storno_count' : {
                        'description': 'A sztornok szama.',
                        'type': 'int'
                    },
                    'cash_topup_amount' : {
                        'description': 'A keszpenzes penz feltoltesek osszege.',
                        'type': 'int'
                    },
                    'cash_topup_count' : {
                        'description': 'A keszpenzes penz feltoltesek szama.',
                        'type': 'int'
                    },
                    'card_topup_amount' : {
                        'description': 'A kartyas penz feltoltesek osszege.',
                        'type': 'int'
                    },
                    'card_topup_count' : {
                        'description': 'A kartyas penz feltoltesek szama.',
                        'type': 'int'
                    },
                    'refund_amount' : {
                        'description': 'A kifizetesek osszege.',
                        'type': 'int'
                    },
                    'refund_count' : {
                        'description': 'A kifizetesek szama.',
                        'type': 'int'
                    }
                }
            }


class LatestTransaction(APIView):
    """
    Az utolsó tranzakció adatai.

    Ha az utolsó tranzakció nem egy vásárlás volt, akkor hibaüzenetet dobunk vissza.
    """

    def post(self, request):
        return Response({
            'amount': 0,
            'timestamp': datetime.datetime.now(),
            'error': 'Some error message!'
        })

    def metadata(self, request):
        """
        Az OPTIONS művelethez biztosít információkat.
        """
        data = super().metadata(request)

        name = data.pop('name')
        description = data.pop('description')

        return {
            'name': name,
            'description': description,
            'input':
                {
                    'device_id': {
                        'description': 'A PDA/terminal azonositoja',
                        'type': 'string'
                    },
                    'bracelet_id': {
                        'description': 'A karperec id-je',
                        'type': 'string'
                    }
                },
            'output':
                {
                    'amount' : {
                        'description': 'A tranzakcio osszege.',
                        'type': 'int'
                    },
                    'timestamp' : {
                        'description': 'A tranzakcio ideje.',
                        'type': 'ISO 8601 DateTime'
                    },
                    'error' : {
                        'description': 'Hibauzenet.',
                        'type': 'string'
                    }
                }
            }