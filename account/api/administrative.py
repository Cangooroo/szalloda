from rest_framework.views import APIView
from rest_framework.response import Response


class Topup(APIView):
    """
    Pénzfeltöltés kártyára.
    """

    def post(self, request):
        return Response({
            'success' : True
        })

    def metadata(self, request):
        """
        Az OPTIONS művelethez biztosít információkat.
        """
        data = super().metadata(request)

        name = data.pop('name')
        description = data.pop('description')

        return {
            'name': name,
            'description': description,
            'input':
                {
                    'device_id': {
                        'description': 'A PDA/terminal azonositoja',
                        'type': 'string'
                    },
                    'bracelet_id': {
                        'description': 'A karperec id-je',
                        'type': 'string'
                    },
                    'amount': {
                        'description': 'A feltoltendo osszeg',
                        'type': 'int'
                    },
                    'payment_type': {
                        'description': 'A feltolteshez hasznalt fizetési mod (keszpenzes, vagy kartyas)',
                        'type': 'string'
                    }
                },
            'output':
                {
                    'success' : {
                        'description': 'A feltoltes sikeresseget jeloli.',
                        'type': 'boolean'
                    }
                }
            }


class Withdraw(APIView):
    """
    Pénz visszafizetés, kártya deaktiválása
    """

    def deactivate_bracelet(self, bracelet_id):
        """
        Karperec deaktiválása
        """
        pass

    def withdraw_balance(self, bracelet_id):
        """
        Pénz levonása, visszafizetés
        """
        pass

    def post(self, request):
        return Response({
            'success' : True,
            'amount' : 0
        })

    def metadata(self, request):
        """
        Az OPTIONS művelethez biztosít információkat.
        """
        data = super().metadata(request)

        name = data.pop('name')
        description = data.pop('description')

        return {
            'name': name,
            'description': description,
            'input':
                {
                    'device_id': {
                        'description': 'A PDA/terminal azonositoja',
                        'type': 'string'
                    },
                    'bracelet_id': {
                        'description': 'A karperec id-je',
                        'type': 'string'
                    }
                },
            'output':
                {
                    'success' : {
                        'description': 'A kifizetes sikeresseget jeloli.',
                        'type': 'boolean'
                    },
                    'amount' : {
                        'description': 'A kifizetendo osszeg (tartalmazza a visszafizetendo dijakat is).',
                        'type': 'int'
                    }
                }
            }

class Storno(APIView):
    """
    Sztornó művelet végrehajtása. Ha van sztornózható tranzakció.
    """

    def post(self, request):
        return Response({
            'success' : True,
            'amount' : 0
        })

    def metadata(self, request):
        """
        Az OPTIONS művelethez biztosít információkat.
        """
        data = super().metadata(request)

        name = data.pop('name')
        description = data.pop('description')

        return {
            'name': name,
            'description': description,
            'input':
                {
                    'device_id': {
                        'description': 'A PDA/terminal azonositoja',
                        'type': 'string'
                    },
                    'bracelet_id': {
                        'description': 'A karperec id-je',
                        'type': 'string'
                    }
                },
            'output':
                {
                    'success' : {
                        'description': 'A muvelet sikeresseget jeloli.',
                        'type': 'boolean'
                    },
                    'amount' : {
                        'description': 'A karkotore visszatoltott osszeg.',
                        'type': 'int'
                    }
                }
            }