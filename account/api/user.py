from rest_framework.views import APIView
from rest_framework.response import Response

# TODO Még meg kell beszélni, hogy pontosan hogyan is működjön

class Login(APIView):
    """
    Login.
    """

    def post(self, request):
        return Response({
            'result': True
        })


class Logout(APIView):
    """
    Logout
    """

    def post(self, request):
        return Response({
            'result': True
        })