from rest_framework.views import APIView
from rest_framework.response import Response


class PayWithValue(APIView):
    """
    Vásárlás kártyával.
    """

    def post(self, request):
        return Response({
            'success' : True,
            'amount' : 0
        })

    def metadata(self, request):
        """
        Az OPTIONS művelethez biztosít információkat.
        """
        data = super().metadata(request)

        name = data.pop('name')
        description = data.pop('description')

        return {
            'name': name,
            'description': description,
            'input':
                {
                    'device_id': {
                        'description': 'A PDA/terminal azonositoja',
                        'type': 'string'
                    },
                    'bracelet_id': {
                        'description': 'A karperec id-je',
                        'type': 'string'
                    },
                    'amount': {
                        'description': 'A tranzakcio osszege',
                        'type': 'int'
                    }
                },
            'output':
                {
                    'success' : {
                        'description': 'A muvelet sikeresseget jeloli.',
                        'type': 'boolean'
                    },
                    'amount' : {
                        'description': 'A tranzakcio osszege.',
                        'type': 'int'
                    }
                }
            }


class PayWithImage(APIView):
    """
    Vásárlás kártyával.
    """

    def post(self, request):
        return Response({
            'success' : True,
            'amount' : 0
        })

    def metadata(self, request):
        """
        Az OPTIONS művelethez biztosít információkat.
        """
        data = super().metadata(request)

        name = data.pop('name')
        description = data.pop('description')

        return {
            'name': name,
            'description': description,
            'input':
                {
                    'device_id': {
                        'description': 'A PDA/terminal azonositoja',
                        'type': 'string'
                    },
                    'bracelet_id': {
                        'description': 'A karperec id-je',
                        'type': 'string'
                    },
                    'image': {
                        'description': 'A tranzakcio osszege kep formajaban.',
                        'type': 'image'
                    }
                },
            'output':
                {
                    'success' : {
                        'description': 'A muvelet sikeresseget jeloli.',
                        'type': 'boolean'
                    },
                    'amount' : {
                        'description': 'A tranzakcio osszege.',
                        'type': 'int'
                    }
                }
            }