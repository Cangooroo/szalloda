from django.db import models
from django.contrib.auth.models import User, AnonymousUser
from django.contrib.auth.hashers import PBKDF2PasswordHasher

from enum import Enum


class Employee(models.Model):
    """
    Egy alkalmazotthoz tartozó adatokat tárol
    """

    # TODO auth

    # Felhasználónév
    username = models.CharField(max_length=25)

    # Jelszó
    password = models.CharField(max_length=120)

    # E-mail cím
    email = models.EmailField()

    _salt = 'VRS4w3s0m3sEcU12eS41T'
    _hasher = PBKDF2PasswordHasher()

    def set_password(self, raw_password):
        """
        Ugyanaz a módszer, mint a beépített django-s User-eknél.
        """
        self.password = self._hasher.encode(raw_password, self._salt)

    def authenticate(cls, username, raw_password):
        """
        Authentikálja a paraméterként kapott adatokat, és visszaadja a megfelelő entitást
        """

        try:
            user = Employee.objects.get(username=username)
            if user.password == cls._hasher.encode(raw_password, cls._salt):
                return user
            else:
                return AnonymousUser
        except cls.DoesNotExist:
            return AnonymousUser


class TransactionFlags(Enum):
    """
    Tranzakció flag-eket tároló Enum
    """

    DEPOSIT_FEE = 1 << 0  # Letéti díj
    COMFORT_FEE = 1 << 1  # Kényelmi díj


class TransactionTypes(Enum):
    """
    Tranzakció t<pusokat tároló Enum
    """

    TOPUP = 'TOPUP'  # Egyenleg feltöltés
    WITHDRAW = 'WITHDRAW'  # Kifizetés
    BUY = 'BUY'  # Vásárlás
    STORNO = 'STORNO'  # Sztornó


class PaymentMethod(Enum):
    """
    Fizetési mód.
    Pénzfeltöltésnél, kifizetésnél lehet cash, vagy card
    Vásárlásnál csak bracelet lehet
    """

    CASH = 'CASH'
    CARD = 'CARD'
    BRACELET = 'BRACELET'


class Account(models.Model):
    """
    Egy karperechez tartozó számla adatai.
    """

    # karperec ID-ja (ami az RFID tag értéke)
    bracelet_id = models.IntegerField(primary_key=True)

    # A karperecen lévő összeg
    balance = models.IntegerField()

    # Azt tárolja, hogy a karperec jelenleg használatban van-e.
    active = models.BooleanField(default=True)

    # Az utolsó aktiválás/deaktiválás dátuma
    activated_at = models.DateTimeField(auto_now=True)

    # TODO blacklisting


class Company(models.Model):
    """
    Egy cég adatait tárolja
    """

    # Cégnév
    name = models.CharField(max_length=30)

    # Cég székhelye
    address = models.CharField(max_length=80)

    # Adószám. Azért Char, mert lehet, hogy egy országban nem csak számokból áll az adószám
    tax_id = models.CharField(max_length=30)


class Device(User):
    """
    Egy eszközhöz tartozó információkat kezel. A Django User osztályát egészíti ki, az authentikációt a Django végzi.
    """

    # TODO auth

    # Fantázia név (ha valamelyik eszközt el akarjuk nevezni)
    fantasy_name = models.CharField(max_length=15)

    # Eszközhöz hozzárendelt alkalmazott
    # assigned_employee = models.ForeignKey(Employee)

    # Eszköz típusa (pincér / recepciós)
    device_type = models.CharField(max_length=10)

    # Cég adatok
    company = models.ForeignKey(Company)


class Transaction(models.Model):
    """
    Egy tranzakció adatai.

    Pozitív érték csökkenti, negatív érték növeli a számla egyenlegét.

    Tranzakció típusok:
        fizetés, feltöltés, kifizetés, sztornó
    Tranzakció flagek:
        kényelmi díj, letéti díj
    """

    # Tranzakció típusok
    TRANSACTION_TYPES = (
        (TransactionTypes.TOPUP.value, 'topup'),  # Egyenleg feltöltés
        (TransactionTypes.WITHDRAW.value, 'withdraw'),  # Egyenleg nullázás, pénz kifizetés
        (TransactionTypes.BUY.value, 'buy'),  # Vásárlás
        (TransactionTypes.STORNO.value, 'storno')  # Szotrnó
    )

    # Tranzakció flagek
    TRANSACTION_FLAGS = (
        (TransactionFlags.DEPOSIT_FEE.value, 'deposit_fee'),  # Letéti díj
        (TransactionFlags.COMFORT_FEE.value, 'comfort_fee')  # Kényelmi díj
    )

    PAYMENT_METHODS = (
        (PaymentMethod.CASH.value, 'cash'),  # Kézpénzes fizetés
        (PaymentMethod.CARD.value, 'card'),  # Bankkártyás fizetés
        (PaymentMethod.BRACELET.value, 'bracelet')  # Karpereces fizetés
    )

    # karperec ID-ja (ami az RFID tag értéke)
    bracelet_id = models.ForeignKey(Account)

    # A tranzakció értéke
    value = models.IntegerField()

    # A tranzakció típusa
    transaction_type = models.CharField(max_length=15, choices=TRANSACTION_TYPES)

    # A tranzakció flag-jei
    transaction_flags = models.IntegerField(choices=TRANSACTION_FLAGS, default=0)

    # Fizetési mód (csak pénzfeltöltésnél lehet cash/bankkártya, vásárlásnál csak bracelet lehet)
    payment_method = models.CharField(max_length=15, choices=PAYMENT_METHODS)

    # Időbélyeg
    timestamp = models.DateTimeField(auto_now=True)

    # Annak az eszköznek az azonosítója, amelyikről küldték
    device_id = models.ForeignKey(Device)

    company = models.ForeignKey(Company)

    # Az alkalmazott(pincér, recepciós, etc.) felhasználóneve
    # TODO user = models.ForeignKey(Employee)


    def has_flag(self, flag):
        """
        Megvizsgálja, hogy a tranzakcióban szerepel-e az adott flag
        :param flag: Ellenőrzendő flag
        :return: Igaz, ha szerepel benne a flag, hamis egyébként
        """
        return bool(self.transaction_flags & flag.value)

    def set_flag(self, flag):
        """
        Beáálítja a megadott flag-et.
        :param flag: A beállítandó flag
        :return:
        """
        self.transaction_flags |= flag.value

    def set_type(self, type):
        """
        A tranzakció típusának beállítása
        :param type: A tranzakció típusa
        :return:
        """
        self.transaction_type = type.value


