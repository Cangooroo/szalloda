from rest_framework import serializers

from .models import Account, Transaction

from django.contrib.auth.models import User

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('bracelet_id', 'balance')

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('bracelet_id', 'full_price', 'timestamp')

# class AccountOperationsSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = AccountOperation
#         fields = ('bracelet_id', 'name', 'personal_id', 'operation_type', 'timestamp')